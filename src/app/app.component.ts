import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  modalRef:any;
  message:any;
  demoForm: FormGroup;
  tickRight: boolean = false;
  tempArr:any = [];
  selectedIndex = -1;



  // modalRef: BsModalRef | undefined;

  constructor(private modalService: BsModalService, private fb: FormBuilder) {
    this.demoForm = this.fb.group({
      fname : [''],
      nname : [''],
      date : [''],
      tick : ['false']
    })
    // this.items = Array(15).fill(0);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
 
  submitForm(): void {
    // this.message = 'Confirmed!';

    this.demoForm.value.tick = 'true';
    this.tempArr.push(this.demoForm.value);
    console.log(this.demoForm.value);

    // localStorage.setItem('tempData' ,  this.tempArr);
    this.modalRef.hide();
    this.demoForm.reset();
  }
 
  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  deleteRow(index:any){
    console.log(index);
    this.tempArr.splice(index, 1);
  }

  markDone(value:any, index:any){
    console.log(value , "value")
    // this.selectedIndex = index;
    console.log("index=",index);
    this.tempArr[index]['tick'] = false
    // this.tempArr[index].push({'tick':this.demoForm.value});
    console.log(this.tempArr , "tempArr")
  }

}

